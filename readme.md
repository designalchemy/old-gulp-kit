# How to use Front End Kit

Series of command line installs that need to be run/qualified

$ gem install breakpoint susy
$ npm install
$ gulp

Edit files inside /src

Files output into /dist

Sass is compiled into /dist/css/styles.css and liveReload'ed into browser

HTML is concatenated on change and auto reloaded

JS/Coffee is compiled, concatenated and pushed to /dist/js/scripts.js

`@include breakpoint($screensize)` to `@include breakpoint(max-width $screensize-max)`
